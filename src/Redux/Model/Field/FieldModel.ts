export interface FieldModel {
    field: PointModel[];
}

export interface PointModel {
    x: number;
    y: number;
}


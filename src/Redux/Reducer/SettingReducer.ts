import { SettingActions } from "../Action";
import { SettingActionType } from "../ActionType/SettingActionType";
import { ISetting, SettingState } from "../Model";

const initialState: SettingState = {
    setting: {} as ISetting
}
const settingReducer = (state: SettingState = initialState, action: SettingActions) => {
    switch (action.type) {
        case SettingActionType.SETTING_GAME:
            return {
                ...state,
                setting: action.payload.clone(state.setting)
            }
        default:
            return state;
    }
}

export default settingReducer;
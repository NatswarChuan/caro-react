import { Dispatch } from "redux";
import { SettingActionType } from "../ActionType/SettingActionType";
import { ISetting } from "../Model";
import { SettingModel } from "../Model/Setting/SettingModel";

export interface SettingGame {
    readonly type: SettingActionType.SETTING_GAME,
    payload: ISetting
}

export type SettingActions = SettingGame;

export const SettingGame = (
    length?: number,
    current?: boolean,
    timing?: number,
    logoX?: string,
    logoY?: string,
    isEndGame?: boolean,
    whoWin?: boolean
) => {
    const payload: SettingModel = new SettingModel(length, current, timing, logoX, logoY, isEndGame, whoWin);
    return (dispatch: Dispatch<SettingActions>) => dispatch({
        type: SettingActionType.SETTING_GAME,
        payload: payload
    })
}
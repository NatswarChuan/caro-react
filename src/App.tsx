import React from 'react';
import logo from './logo.svg';
import './App.css';
import Field from './Components/Field/Field';

function App() {
  return (
    <div className="App">
      <Field></Field>
    </div>
  );
}

export default App;

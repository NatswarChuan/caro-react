import { ISetting } from "./SettingModel";

export interface SettingState {
    setting: ISetting;
}
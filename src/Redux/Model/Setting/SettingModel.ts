export interface ISetting {
    length: number;
    current: boolean;
    timing: number;
    logoX: string;
    logoY: string;
    isEndGame: boolean;
    whoWin: boolean;

    clone(setting: SettingModel): ISetting;
}

export class SettingModel implements ISetting {
    length: number;
    current: boolean;
    timing: number;
    logoX: string;
    logoY: string;
    isEndGame: boolean;
    whoWin: boolean;

    constructor(
        length?: number,
        current?: boolean,
        timing?: number,
        logoX?: string,
        logoY?: string,
        isEndGame?: boolean,
        whoWin?: boolean
    ) {
        this.length = length || 20;
        this.current = current || true;
        this.timing = timing || 30000;
        this.logoX = logoX || "X";
        this.logoY = logoY || "Y";
        this.isEndGame = isEndGame || true;
        this.whoWin = whoWin || true;
    }

    clone(setting: SettingModel): ISetting {
        this.length = setting.length;
        this.current = setting.current;
        this.timing = setting.timing;
        this.logoX = setting.logoX;
        this.logoY = setting.logoY;
        this.isEndGame = setting.isEndGame;
        this.whoWin = setting.whoWin;

        return this;
    }
}


import { combineReducers } from 'redux';
import settingReducer from './SettingReducer';
const rootReducer = combineReducers({
    settingReducer,
});

export default rootReducer;

export type State = ReturnType<typeof rootReducer>